from django.db import models

# Create your models here.

class UserDetails(models.Model):
    name=models.CharField(max_length=100)
    Shopname=models.CharField(max_length=100)
    Status=models.CharField(max_length=100)

    def __str__(self):
        return self.name
