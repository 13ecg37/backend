from rest_framework import  serializers
from . models import UserDetails

# Lead Serializers

class UserDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDetails
        fields="__all__"