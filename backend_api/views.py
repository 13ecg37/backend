
from . models import UserDetails
from . serializer import UserDetailsSerializer


from rest_framework import viewsets

class UserViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """
    serializer_class = UserDetailsSerializer
    queryset = UserDetails.objects.all()